import matplotlib.pyplot as plt
import pandas as pd

from data import Data
from model import mongo_database, mongo_collection, Keras_Model
from model.lstmmodel import LSTM_Model


def main():
    value = ''
    while value != 'q':
        value = input("1-learn, 2-predict 3-update data (q to quit) :")
        if value == '3':
            data_monde = Data('corona_monde')
            data_monde.plot_results('Pays')
            data_europe = Data('corona_europe')
            data_europe.plot_results('Pays')
            data_prediction = Data('corona_prediction')
            data_prediction.plot_results('Pays')
            data_prediction_2 = Data('corona_prediction_2')
            data_prediction_2.plot_results('Pays')

        elif value == '1':
            train_data = Data('corona_europe')
            print(train_data.selected_rows_dict)
            country_idx = int(input("choose a country :"))
            row_mask_var = "data['Pays']=='" + train_data.selected_rows_dict[country_idx] + "'"
            train_data.dataset = train_data.select_data(train_data.dataset, row_mask=row_mask_var)
            print(train_data.selected_columns_dict)
            field_idx = int(input("choose a field :"))

            model = LSTM_Model(train_data, columns_features=[train_data.selected_columns_dict[field_idx]])
            model.build_model('LSTM')
            train_features, train_labels, test_features, test_labels = model.get_train_test_data(0.75,
                                                                                                 method='random',
                                                                                                 normalize=True)
            model.train(train_features, train_labels, test_features, test_labels, epochs=1000,
                        monitor='mean_squared_error',
                        early_stopping=False,
                        tensorboard_logging=False)
            model.display_history(history_type=2)
            model.save_model(
                train_data.selected_rows_dict[country_idx] + '_' + train_data.selected_columns_dict[field_idx])

        elif value == '2':
            data_to_predict = Data('corona_europe')
            model_dict = Keras_Model.get_model_list(mongo_database, mongo_collection, 'LSTM')
            print(model_dict)
            model_idx = input("choose a model :")
            model_id = model_dict[int(model_idx)]
            model = Keras_Model.load_model(model_id, mongo_database, mongo_collection)
            print(data_to_predict.selected_rows_dict)
            country_idx = int(input("choose a country :"))
            row_mask_var = "data['Pays']=='" + data_to_predict.selected_rows_dict[country_idx] + "'"
            data_to_predict.dataset = data_to_predict.select_data(data_to_predict.dataset, row_mask=row_mask_var)
            predict = model.predict(data_to_predict, columns_features=model.columns_features)
            result = pd.concat(predict, axis=1, sort=False)
            result.plot()
            plt.title(data_to_predict.selected_rows_dict[country_idx]+'  prediction')
            plt.show()


if __name__ == '__main__':
    main()
