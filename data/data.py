import json
import logging
import math
import os
from importlib import import_module

import matplotlib.pyplot as plt
import matplotlib.style as pltstyle
import numpy as np
import pandas as pd
import redis
import requests
from pandas.io.json import json_normalize
from sklearn.preprocessing import MinMaxScaler

from data import configs, file_directory
from util import Timer, Logger


class Data:
    selected_columns = None
    selected_columns_dict = None
    selected_rows = None
    selected_rows_dict = None
    rows_masks = None
    date_to_day = None
    method = None
    dataset = None

    def __init__(self, data_source, data_frame=False):
        timer = Timer()
        timer.start()
        self.logger = Logger("data", logging.INFO).get_logger()
        if data_frame:
            self.dataset = data_source
        else:
            config_data = configs['sources'][data_source]
            file_data = config_data['file_name'] if 'file_name' in config_data else None
            data_type = config_data['type'] if 'type' in config_data else None
            file_sep = config_data['separator'] if 'separator' in config_data else ';'
            skip_rows = config_data['skip_rows'] if 'skip_rows' in config_data else 0

            self.selected_columns = config_data['selected_columns'] if 'selected_columns' in config_data else None
            self.selected_columns_dict = {i: self.selected_columns[i] for i in range(0, len(
                self.selected_columns))} if self.selected_columns is not None else None
            self.selected_rows = config_data['selected_rows'] if 'selected_rows' in config_data else None
            self.selected_rows_dict = {i: self.selected_rows['row_values'][i] for i in
                                       range(0, len(
                                           self.selected_rows[
                                               'row_values']))} if self.selected_rows is not None else None

            self.rows_masks = config_data['rows_masks'] if 'rows_masks' in config_data else None
            self.date_to_day = config_data['convert_date_to_day'] \
                if 'convert_date_to_day' in config_data else None
            self.method = config_data['method'] if 'method' in config_data else None

            connection = config_data['connection'] if 'connection' in config_data else None

            if connection is not None and type(connection) != str:
                self.dataset = self.load_data_db(connection, data_type)
            else:
                if file_data is not None and \
                        not os.path.exists(file_directory + file_data):
                    self.download_file(connection, file_data)
                self.dataset = self.load_data_file(file_data, data_type,
                                                   file_sep=file_sep,
                                                   skiprows=skip_rows)
            # TODO to remove
            # self.train_features, self.train_label = self.get_features_labels(self.dataset,
            #                                                                  self.columns_features,
            #                                                                  self.columns_labels)
        self.logger.info("[Data] Data loaded")
        timer.stop()

    def prepare_data(self, data_var):
        if self.rows_masks is not None:
            for mask_array in self.rows_masks:
                mask_exact = pd.eval("data_var['" + mask_array[0] + "']" + mask_array[1] + mask_array[2])
                data_var = data_var[mask_exact]
            self.logger.info("[Prepare Data] Row Masks")
        if self.selected_rows is not None:
            mask_is_in = ""
            idx_len = len(self.selected_rows['row_values']) - 1
            for idx, value in enumerate(self.selected_rows['row_values']):
                mask_is_in = mask_is_in + "data_var[self.selected_rows['column']] =='" + value + "'"
                if idx_len != idx:
                    mask_is_in = mask_is_in + " or "
            data_var = data_var[pd.eval(mask_is_in)]
            self.logger.info("[Prepare Data] Select Rows")
        if self.selected_columns is not None:
            columns_list = self.selected_columns
            data_var = pd.DataFrame(data=data_var, columns=columns_list)
            self.logger.info("[Prepare Data] Select Columns")
        if self.date_to_day is not None:
            data_var = self.convert_date_to_day(data_var, self.date_to_day)
        return data_var

    def select_data(self, data, row_mask=None):
        if row_mask is not None:
            data = data[pd.eval(row_mask)]
        self.logger.info("[Select Data] Select Rows")
        return data

    def load_data_db(self, connection_db, db_type):
        if db_type == "redis":
            client = redis.Redis(host=connection_db['host'],
                                 port=connection_db['port'],
                                 db=int(connection_db['database']))
            key_numbers = int(client.get(connection_db['database_size']).decode("utf-8"))
            thedict = []
            for key in range(1, key_numbers + 1):
                record = {}
                for column in self.selected_columns:
                    value = str(client.hget(key.__str__(), column), 'utf-8')
                    record[column] = value
                thedict.insert(key, record)
            dfdata = pd.DataFrame(thedict)
            client.close()
            # TODO to remove
            # if self.rows_masks is not None:
            #     dfdata = self.select_data(dfdata, self.rows_masks)
            return dfdata

    def load_data_file(self, file_data_var, file_type_var, file_sep=';', skiprows=None):

        if file_type_var == "json":
            file = open(file_directory + file_data_var['file_name'], "r")
            data = json.load(file)
            dfdata = json_normalize(data)
            dfdata = self.prepare_data(dfdata)
            self.logger.info("load file:" + file_directory + file_data_var)

        elif file_type_var == "csv":
            self.logger.info("load file:" + file_directory + file_data_var)
            dfdata = pd.read_csv(file_directory + file_data_var, sep=file_sep, skiprows=skiprows,
                                 parse_dates=True)
            dfdata = self.prepare_data(dfdata)


        elif file_type_var == "npz_pickle_dict":
            with np.load(file_directory + file_data_var, allow_pickle=True) as f:
                self.logger.info("load file:" + file_directory + file_data_var)
                dfdata = pd.DataFrame(data=f['y_train'], index=f['x_train'])
                # TODO a améliorer
                # return f['x_train'], f['y_train'], None

        else:
            self.logger.info("can not load:" + file_directory + file_data_var)
            return None
        # TODO to remove
        # if self.rows_masks is not None:
        #     dfdata = self.select_data(dfdata, self.rows_masks)

        return dfdata

    def download_file(self, connexion_var, file_data_var):
        the_file = requests.get(connexion_var)
        open(file_directory + file_data_var, 'wb').write(the_file.content)
        self.logger.info("download:" + file_data_var)

    @staticmethod
    def get_split_sample(data_var_x, data_var_y, test_split):
        i_split = int(len(data_var_x) * test_split)
        return data_var_x[0:i_split], data_var_y[0:i_split], data_var_x[i_split:], data_var_y[i_split:]

    @staticmethod
    def get_ramdom_sample(data_var_x, data_var_y, test_split):
        i_split = int(len(data_var_x) * test_split)
        indices = np.sort(np.random.choice(len(data_var_x), i_split))
        xtest = []
        ytest = []
        for i in indices:
            xtest.append(data_var_x[i])
            ytest.append(data_var_y[i])
        return data_var_x, data_var_y, xtest, ytest

    @staticmethod
    def normalize_data(data_array_var, method):
        module = import_module('sklearn.preprocessing')
        scalar = getattr(module, method)()
        return scalar.fit_transform(data_array_var)

        # numpy_array = np.array(data_array_var)
        # transpose = numpy_array.T
        # transpose_array = transpose.tolist()
        # # TODO remove
        # #print('transpose_array=',transpose_array)
        # transpose_normalized_array=[]
        # for row in transpose_array:
        #     valuemin = min(row)
        #     valuemax = max(row)
        #     row = [(element - valuemin) / (valuemax - valuemin) for element in row]
        #     #print('normalized row=',row)
        #     transpose_normalized_array.append(row)
        # numpy_array = np.array(transpose_normalized_array)
        # detranspose = numpy_array.T
        # detranspose_array = detranspose.tolist()
        # return detranspose_array

    @staticmethod
    def normalize_windows(data_feature, window_size):
        data_x = []
        data_y = []
        data_c = []
        for i in range(len(data_feature) - window_size):
            x, y, c = Data.generate_samples(i, data_feature, window_size)
            data_x.append(x)
            data_y.append(y)
            data_c.append(c)
        data_x = np.array(data_x)
        data_y = np.array(data_y)
        data_x = data_x.reshape(data_x.shape[0], data_x.shape[1], 1)  # X.reshape(samples, timesteps, features)
        return data_x, data_y

    @staticmethod
    def generate_samples(i, y_var, window_size):
        window = y_var[i:i + window_size]
        # print('window=', window)
        normalised_window = []
        for p in window:
            normalised_value = ((float(p) / float(window[0])) - 1)
            normalised_window.append(normalised_value)
        x = normalised_window[:-1]
        y = normalised_window[-1]
        c = float(window[-1]) / float(y + 1)
        # print('generate window y=', y,' x=', x, 'coeff=',c,'true_y=',c*(y+1))
        return x, y, c

    def convert_date_to_day(self, data_var, convert_field):
        columns_list = data_var.columns
        columns_list.insert(0, 'Days')
        data_by_columns_var = pd.DataFrame(columns=columns_list)
        data_var = data_var.sort_values(by='Date')
        for field in self.selected_rows_dict.values():
            maskcolumn = data_var[convert_field] == field
            data_var_column = data_var[maskcolumn]
            data_var_column['next_date_value'] = data_var_column['Date'].shift(-1)
            row_iterator = data_var_column.itertuples()
            day = 1
            days = []
            for row in row_iterator:
                if row.Date != row.next_date_value:
                    days.append(day)
                    day = day + 1
                else:
                    days.append(day)
            data_var_column['Days'] = days
            data_var_column.drop(columns=['next_date_value'], inplace=True)
            data_by_columns_var = data_by_columns_var.append(data_var_column, sort=False)
            self.logger.debug('Add ' + convert_field)

        data_by_columns_var['Days'] = data_by_columns_var['Days'].astype(int)
        data_by_columns_var.set_index(['Days'], inplace=True)
        data_by_columns_var.drop(columns=['Date'], inplace=True)
        self.columns_features = data_by_columns_var.columns
        self.selected_columns_dict = {i: self.columns_features[i] for i in range(0, len(
            self.columns_features))}
        return data_by_columns_var

    def get_data_by_field(self, columns_var):
        if columns_var is not None:
            data_var = self.dataset.pivot(columns=columns_var)
        else:
            data_var = self.dataset
        result_dict = {}
        for field in self.selected_columns_dict.values():
            if field != columns_var:
                data_field = data_var[field]
                result_dict.update({'Nbr ' + field: data_field})
                self.logger.debug('Add ' + field)
        return result_dict

    def plot_results(self, columns_var=None):
        pltstyle.use('ggplot')
        data_dictionnary = self.get_data_by_field(columns_var)
        dictsize = len(data_dictionnary)
        lines = math.ceil(dictsize / 2)
        if lines == 0:
            lines = 1
        if dictsize == 1:
            columns = 1
        else :
            columns = 2
            fig, axes = plt.subplots(nrows=lines, ncols=columns, facecolor='white')
        li = 0
        col = 0
        for data_type, data in data_dictionnary.items():
            if columns == 1:
                ax = plt.gca()
            else:
                if lines == 1:
                    ax = axes[col]
                else:
                    ax = axes[li, col]
            data.plot(ax=ax)
            ax.set_xlabel('Nbr of Days', fontsize=8)
            ax.set_ylabel(data_type, fontsize=8)
            ax.set_title(data_type + ' by days', fontsize=8)
            ax.tick_params(axis='both', which='major', labelsize=8)
            ax.legend(loc='upper left', frameon=False, fontsize='x-small')

            self.logger.info("plot:" + data_type + " position:" + str(li) + ":" + str(col))

            col = col + 1
            if col > 1:
                col = 0
                li = li + 1
        plt.tight_layout()
        plt.show()
