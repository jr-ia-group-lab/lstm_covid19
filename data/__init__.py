import json

file_directory = './data/files/'
config_file = './data/config/config.json'
with open(config_file, 'r', encoding="utf-8") as config_file:
    configs = json.load(config_file)
from data.data import *
