import logging

from data import Data
from model import Keras_Model
from util import Timer, Logger
import numpy as np
import pandas as pd


class ANN_Model(Keras_Model):
    def __init__(self):
        super().__init__()
        self.logger = Logger('Model ANN', logging.INFO).get_logger()

    def get_train_test_data(self, test_split=1.0, method=None, normalize=True):
        timer = Timer()
        timer.start()
        features, labels, ftest, ltest = super().get_train_test_data(test_split, method, normalize)
        timer.stop()
        self.logger.info(
            'Get train and test normalize=' + str(normalize) + ' data with method ' + method + ' value=' + str(
                test_split))
        return features, labels, ftest, ltest

    def predict(self, data: Data, normalize=True, from_train_data=True):
        # ANN predicts from x axis of data object and return the predicted dataset by populating the y axis
        timer = Timer()
        timer.start()
        # data_var = data.dataset
        data_var = data.dataset.iloc[:int(data.dataset.size / 2)]

        train_data = self.train_data.dataset
        if from_train_data:
            xmax = train_data.index.values.max()
            print(xmax)
            xmin = train_data.index.values.min()
            print(xmin)
            xpredict = list(set(train_data.index).symmetric_difference(set(data_var.index)))
        else:
            xpredict = data_var.index.values
            xmax = data_var.index.values.max()
            xmin = data_var.index.values.min()

        if normalize:
            # start normalize
            xpredict = (xpredict - xmin) / (xmax - xmin)

        ypredict = (self.model.predict(xpredict).flatten())

        if normalize:
            # start denormalize
            ymax = data_var.values.max()
            ymin = data_var.values.min()
            xpredict = (xpredict * (xmax - xmin)) + xmin
            ypredict = (ypredict * (ymax - ymin)) + ymin

        predict = pd.DataFrame({'Days': xpredict.round(), data_var.columns[0]: ypredict})
        predict['Days'] = predict['Days'].astype(np.int64)
        predict.set_index('Days', inplace=True)
        timer.stop()
        self.logger.info('predict ANN ' + str(predict.size))
        if from_train_data:
            return [predict, data_var, data.dataset, train_data]
        else:
            return [predict, data.dataset, data_var]
