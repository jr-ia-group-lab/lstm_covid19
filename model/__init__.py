import warnings

warnings.filterwarnings("ignore")
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)

import logging
logging.getLogger('tensorflow').disabled = True

model_directory = './model/'
config_directory = './model/config/'
mongo_database = 'datascience'
mongo_collection = 'models'

from model.keras_model import Keras_Model
from model.annmodel import ANN_Model

