import logging

import numpy as np
import pandas as pd

from data import Data
from model import Keras_Model
from util import Logger, Timer


class LSTM_Model(Keras_Model):

    def __init__(self, data: Data = None, columns_features=None, columns_labels=None):
        super().__init__(data, columns_features, columns_labels)
        self.logger = Logger('Model LSTM', logging.INFO).get_logger()

    def get_train_test_data(self, test_split=1.0, method=None, normalize=True):
        timer = Timer()
        timer.start()
        features, labels, self.train_data.dataset = self.get_train_data(self.train_data.dataset,
                                                                        self.columns_features,
                                                                        self.columns_labels)
        features, labels = Data.normalize_windows(features, self.window_size)
        features, labels, ftest, ltest = self.get_test_data(method, test_split, features, labels)
        timer.stop()
        self.logger.info(
            'Get train and test normalize=' + str(normalize) + ' data with method ' + method + ' value=' + str(
                test_split))
        return [features], labels, [ftest], ltest

    def predict(self, data: Data, prediction_split=0.25, columns_features=None, columns_labels=None):
        timer = Timer()
        timer.start()
        features, labels, initial_data = self.get_train_data(data.dataset,
                                                             columns_features,
                                                             columns_labels)
        prediction_size = int(self.train_data.dataset.size * (1 - prediction_split))
        data_var = initial_data.iloc[:prediction_size]
        window = data_var.values[-(self.window_size - 1):]
        data_to_predict = initial_data.iloc[prediction_size:]
        prediction = []

        for value in data_to_predict.values:
            coeff = float(window[0])
            window_to_predict = (window / coeff) - 1
            window_to_predict = window_to_predict.reshape(1, window_to_predict.shape[0], 1)
            print(window_to_predict)
            prediction_value = self.model.predict(window_to_predict)
            prediction_value = np.reshape(prediction_value, prediction_value.size)
            prediction.append((prediction_value[0] + 1) * coeff)
            window = np.append(window, value)
            window = window[-(self.window_size - 1):]

        for i in range(len(self.train_data.dataset.index) - len(data.dataset.index) + 2):
            coeff = float(window[0])
            window_to_predict = (window / coeff) - 1
            window_to_predict = window_to_predict.reshape(1, window_to_predict.shape[0], 1)
            prediction_value = self.model.predict(window_to_predict)
            prediction_value = np.reshape(prediction_value, prediction_value.size)
            prediction.append((prediction_value[0] + 1) * coeff)
            window = np.append(window, (prediction_value[0] + 1) * coeff)
            window = window[-(self.window_size - 1):]

        index = np.arange(prediction_size - 1, len(prediction) + prediction_size - 1)

        predict = pd.DataFrame(data=prediction, index=index, columns=["Prediction_" + columns_features[0]])

        train_data = self.train_data.dataset.copy()
        train_data.rename(columns={train_data.columns[0]: "Train_" + columns_features[0]},
                          inplace=True)
        timer.stop()
        self.logger.info('predict LSTM ' + str(predict.size))
        return [train_data, initial_data, predict]
