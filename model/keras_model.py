import json
import logging
import pickle
import pprint
from datetime import datetime
from importlib import import_module

import pandas as pd

import keras
import matplotlib.pyplot as plt
import numpy as np
from keras import metrics
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Dropout, LSTM, Conv2D, MaxPooling2D, Flatten
from keras.models import Sequential
from pymongo import MongoClient
from data import Data
from model import model_directory, mongo_database, mongo_collection, config_directory
from util import Timer, Logger


class Keras_Model:
    name = None
    type = None
    logger = None

    window_size = None
    vocabulary = None
    vectorizer = None
    history = None

    model = None
    loss = None
    optimizer = None
    metrics = None

    train_data: Data = None
    columns_labels = None
    columns_features = None

    encoder = None
    decoder = None

    def __init__(self, data: Data = None, columns_features=None, columns_labels=None):
        self.logger = Logger('Model', logging.INFO).get_logger()
        self.name = self.__class__.__name__
        self.type = self.__class__.__module__
        self.saved_directory = model_directory + "saved_models/"
        self.log_directory = model_directory + "tensorboard_logs/" + datetime.now().strftime("%Y%m%d-%H%M%S")
        self.columns_features = columns_features
        self.columns_labels = columns_labels
        self.train_data = data

    def build_model(self, model_name, configs=None):
        timer = Timer()
        timer.start()

        if configs is None:
            with open(config_directory + 'config' + model_name + '.json', 'r') as config_file:
                configs = json.load(config_file)

        self.name = configs['model']['name'] if self.name is None else self.name
        self.type = configs['model']['type'] if self.type is None else self.type
        self.logger.info('Build Model ' + self.name + ' of type ' + self.type)

        self.model = Sequential()
        self.loss = configs['model']['loss'] if 'loss' in configs['model'] else None
        self.optimizer = configs['model']['optimizer'] if 'optimizer' in configs['model'] else None
        self.metrics = [metrics.mae, metrics.mse]

        for layer in configs['model']['layers']:

            neurons = self.evaluate_parameter(layer, 'neurons')
            dropout_rate = layer['rate'] if 'rate' in layer else None
            activation = layer['activation'] if 'activation' in layer else None
            return_seq = layer['return_seq'] if 'return_seq' in layer else None
            input_timesteps = layer['input_timesteps'] if 'input_timesteps' in layer else None
            input_dim = self.evaluate_parameter(layer, 'input_dim')
            filters = layer['filters'] if 'filters' in layer else None
            conv_window_height = layer['conv_window_height'] if 'conv_window_height' in layer else None
            conv_window_width = layer['conv_window_width'] if 'conv_window_width' in layer else None
            input_shape_rows = layer['input_shape_rows'] if 'input_shape_rows' in layer else None
            input_shape_cols = layer['input_shape_cols'] if 'input_shape_cols' in layer else None
            input_shape_channels = layer['input_shape_channels'] if 'input_shape_channels' in layer else None
            pool_size_vertical = layer['pool_size_vertical'] if 'pool_size_vertical' in layer else None
            pool_size_horizontal = layer['pool_size_horizontal'] if 'pool_size_horizontal' in layer else None

            if layer['type'] == 'dense':
                self.model.add(Dense(neurons, activation=activation, input_dim=input_dim))
            if layer['type'] == 'lstm':
                self.model.add(LSTM(neurons, input_shape=(input_timesteps, input_dim), return_sequences=return_seq))
            if layer['type'] == 'dropout':
                self.model.add(Dropout(dropout_rate))
            if layer['type'] == 'Conv2D':
                self.model.add(Conv2D(filters, kernel_size=(conv_window_height, conv_window_width),
                                      input_shape=(input_shape_rows, input_shape_cols, input_shape_channels),
                                      activation=activation))
            if layer['type'] == 'MaxPooling2D':
                self.model.add(MaxPooling2D(pool_size=(pool_size_vertical, pool_size_horizontal)))
            if layer['type'] == 'flatten':
                self.model.add(Flatten())

            self.window_size = input_timesteps + 1 if input_timesteps is not None else self.window_size

        if self.loss is not None and self.optimizer is not None:
            self.model.compile(loss=self.loss,
                               optimizer=self.optimizer,
                               metrics=self.metrics)
            print(self.model.summary())
            self.logger.info('Model Compiled with loss=' + self.loss + ' optimizer=' + self.optimizer)
        timer.stop()
        return self.model

    def get_train_test_data(self, test_split=1.0, test_method=None, normalize_method=None):
        train_features, train_labels, self.train_data.dataset = self.get_train_data(self.train_data.dataset,
                                                                                    self.columns_features,
                                                                                    self.columns_labels)
        if normalize_method is not None:
            train_features = Data.normalize_data(train_features, normalize_method)
        if test_method is not None:
            features, labels, ftest, ltest = self.get_test_data(test_method, test_split, train_features, train_labels)
            return features, labels, ftest, ltest
        else:
            return train_features, train_labels, None, None

    @staticmethod
    def get_train_data(dataset, columns_features, columns_labels):
        if columns_features is not None:
            train_features = dataset[columns_features].values
            columns_var = columns_features
        else:
            train_features = dataset.value
            columns_var = dataset.columns

        if columns_labels is not None:
            train_labels = dataset[columns_labels].values
            labels_dataframe = pd.DataFrame(data=train_labels, columns=columns_labels)
            columns_dataframe = pd.DataFrame(data=train_features, columns=columns_var)
            dataset = pd.concat([columns_dataframe, labels_dataframe], axis=1, sort=False)
        else:
            train_labels = dataset.index.values
            dataset = pd.DataFrame(data=train_features, index=train_labels, columns=columns_var)
        return train_features, train_labels, dataset

    @staticmethod
    def get_test_data(method, test_split, features, labels):
        ftest = None
        ltest = None
        if method == 'split':
            features, labels, ftest, ltest = Data.get_split_sample(features, labels, test_split)
        elif method == 'random':
            features, labels, ftest, ltest = Data.get_ramdom_sample(features, labels, test_split)
        return features, labels, ftest, ltest

    def train(self, x, y, xtest=None, ytest=None, epochs=1000, batch_size=32, tensorboard_logging=False,
              early_stopping=False,
              monitor='val_loss',
              min_delta=0, patience=2, verbose=1):
        timer = Timer()
        timer.start()
        self.logger.info('Model ' + self.name + ' Training Started')
        self.logger.info('Model ' + self.name + '[Model] %s epochs, %s batch size' % (epochs, batch_size))
        callbacks = []
        stopped_epoch = epochs
        if tensorboard_logging:
            tensorboard_callback = keras.callbacks.TensorBoard(
                log_dir=self.log_directory + '_' + self.name + '_' + str(epochs))
            callbacks.append(tensorboard_callback)
        if early_stopping:
            self.logger.info('Model ' + self.name + '[EarlyStopping] %s monitor, %s min_delta' % (monitor, min_delta))
            early_stopping_monitor = EarlyStopping(monitor=monitor, min_delta=min_delta, patience=patience, verbose=1)
            callbacks.append(early_stopping_monitor)
            stopped_epoch = early_stopping_monitor.stopped_epoch
        if xtest is not None and ytest is not None:
            history = self.model.fit(x, y,
                                     validation_data=(xtest, ytest),
                                     epochs=epochs,
                                     batch_size=batch_size,
                                     callbacks=callbacks,
                                     verbose=verbose)
            if verbose == 1:
                loss_and_metrics = self.model.evaluate(xtest, ytest, batch_size=batch_size)
                if len(loss_and_metrics) != 0:
                    i = 0
                    for metric in self.model.metrics_names:
                        print(metric, '=', loss_and_metrics[i])
                        i = i + 1
        else:
            history = self.model.fit(x, y,
                                     epochs=epochs,
                                     batch_size=batch_size,
                                     callbacks=callbacks,
                                     verbose=verbose)
        self.history = history.history

        self.logger.info(
            "model training " + self.name + " for batch size " + str(batch_size) + " and epoch " + str(stopped_epoch))
        self.logger.info('[Model] Training Completed')
        timer.stop()

    def display_history(self, history_type=1):
        pp = pprint.PrettyPrinter(width=41, compact=True)

        if history_type == 1:
            pp.pprint(self.history)
        elif history_type == 2:
            for metric in self.model.metrics_names:
                plt.plot(self.history[metric])
                # plt.plot(self.history['val_' + metric])
                plt.title('Model ' + metric)
                plt.ylabel(metric)
                plt.xlabel('Epoch')
                plt.legend(['Train', 'Test'], loc='upper left')
                plt.show()

    def display_model_layers(self, bins=500):
        for layer in self.model.layers:
            weights = layer.get_weights()
            print('layer:', layer.name, ' with ', len(weights), ' weights and shape :')
            for weight in weights:
                print(weight.shape)
                plt.hist(np.ndarray.flatten(weight), bins=bins)
                plt.show()

    def save_model(self, parameters):
        timer = Timer()
        timer.start()

        client = MongoClient()
        db = client[mongo_database]
        model_collection = db[mongo_collection]
        pickle_model = pickle.dumps(self.model)
        pickle_data = pickle.dumps(self.train_data)

        saved_model = {
            "_id": self.name + '_' + parameters,
            "model": pickle_model,
            "columns_labels": self.columns_labels,
            "columns_features": self.columns_features,
            "type": self.__class__.__name__,
            "module": self.__module__,
            "date": datetime.now()
        }

        if self.__class__.__name__ == 'LSTM_Model':
            saved_model["window_size"] = self.window_size
            saved_model["train_data"] = pickle_data

        elif self.__class__.__name__ == 'NLU_ANN_Model':
            saved_model["vocabulary"] = self.vocabulary
            saved_model["train_data"] = pickle_data

        elif self.__class__.__name__ == 'AUTOENCODE_Model':
            pickle_encoder = pickle.dumps(self.encoder)
            saved_model["encoder"] = pickle_encoder
            pickle_decoder = pickle.dumps(self.decoder)
            saved_model["decoder"] = pickle_decoder
        model_collection.update_one({'_id': self.name + '_' + parameters}, {"$set": saved_model}, upsert=True)
        self.logger.info('Upsert model ' + self.name + '_' + parameters + ' to db')

        client.close()
        self.model.save(self.saved_directory + self.name + '_' + parameters + '.h5')
        timer.stop()
        self.logger.info('save ' + self.saved_directory + self.name + '_' + parameters + '.h5')

    @staticmethod
    def get_model_list(mongo_database_var, mongo_collection_var, filter_type=None):
        timer = Timer()
        timer.start()
        client = MongoClient()
        i = 1
        model_dict = {}
        for model in client[mongo_database_var][mongo_collection_var].find():
            if filter_type is None:
                model_dict.update({i: model["_id"]})
                i = i + 1
            else:
                if filter_type in model["type"]:
                    model_dict.update({i: model["_id"]})
                    i = i + 1
        client.close()
        timer.stop()
        return model_dict

    @staticmethod
    def load_model(model_id, mongo_database_var, mongo_collection_var):
        timer = Timer()
        timer.start()
        client = MongoClient()
        model_json = client[mongo_database_var][mongo_collection_var].find_one({"_id": model_id})
        class_name = model_json['type']
        module_name = model_json['module']
        module = import_module(module_name)
        model_object = getattr(module, class_name)()
        model_object.model = pickle.loads(model_json['model'])
        model_object.name = model_id.split('_')[0]

        model_object.columns_features = model_json['columns_features']
        model_object.columns_labels = model_json['columns_labels']

        if class_name == 'LSTM_Model':
            model_object.window_size = model_json['window_size']
            model_object.train_data = pickle.loads(model_json['train_data'])
        elif class_name == 'NLU_ANN_Model':
            model_object.vocabulary = model_json['vocabulary']
            model_object.train_data = pickle.loads(model_json['train_data'])
        elif class_name == 'AUTOENCODE_Model':
            model_object.encoder = pickle.loads(model_json['encoder'])
            model_object.decoder = pickle.loads(model_json['decoder'])

        client.close()
        timer.stop()
        model_object.logger.info('Load model ' + model_object.name + ' from db')
        return model_object

    @staticmethod
    def evaluate_parameter(json_string, json_field):
        if json_field in json_string:
            json_value = json_string[json_field]
            if isinstance(json_value, str):
                print("EVAL=" + json_value)
                return eval(json_value)
            else:
                return json_value
        else:
            return None
