import matplotlib.pyplot as plt
import pandas as pd

from data import Data
from model import ANN_Model, mongo_database, mongo_collection, Model


def main():
    value = ''
    data = Data('corona')
    while value != 'q':
        value = input("1-learn, 2-predict 3-update data (q to quit) :")
        if value == '3':
            data.plot_results('Pays')
        elif value == '1':
            print(data.selected_rows_dict)
            country_idx = int(input("choose a country :"))
            print(data.selected_columns_dict)
            field_idx = int(input("choose a field :"))

            train_data = Data('corona',
                              row_mask_var="data['Pays']=='" + data.selected_rows_dict[country_idx] + "'",
                              columns_list_var=[data.selected_columns_dict[field_idx]])

            model = ANN_Model()
            model.build_keras_model('ANNClassifier2', train_data)
            train_features, train_labels, test_features, test_labels = model.get_train_test_data(train_data,
                                                                                                 0.75,
                                                                                                 method='split',
                                                                                                 normalize=True)
            model.train(train_features, train_labels, test_features, test_labels, epochs=1000,
                        monitor='mean_squared_error',
                        early_stopping=False)
            model.display_history(history_type=2)
            model.save_model(data.selected_rows_dict[country_idx] + '_' + data.selected_columns_dict[field_idx])

        elif value == '2':
            model_dict = Model.get_model_list(mongo_database, mongo_collection, 'ANN')
            print(model_dict)
            model_idx = input("choose a model :")
            model_id = model_dict[int(model_idx)]
            model = Model.load_model(model_id, mongo_database, mongo_collection)
            train_data = model.train_data.dataset
            print(data.selected_rows_dict)
            country_idx = int(input("choose a country :"))
            country = data.selected_rows_dict[country_idx]
            data_to_predict = Data('corona', row_mask_var="data['Pays']=='" + country + "'",
                                   columns_list_var=train_data.columns)
            predict = model.predict(data_to_predict, normalize=True, from_train_data=True)
            result = pd.concat(predict, axis=1, sort=False)
            result.plot()
            plt.title(model_id + ' prediction')
            plt.show()


if __name__ == '__main__':
    main()
